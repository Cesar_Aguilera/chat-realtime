<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoomRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Room;

class RoomController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) : \Illuminate\View\View
    {
        $rooms = Room::paginate(20);

        return view('rooms.index', compact('rooms'));
    }

    /**
     * @param Request $request
     * @param Room $room
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request, Room $room) : \Illuminate\View\View
    {
        return View::make('rooms.show', compact('room'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create() : \Illuminate\View\View
    {
        return View::make('rooms.create');
    }

    /**
     * @param CreateRoomRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRoomRequest $request) : RedirectResponse
    {
        Room::create([
            'name' => $request->name,
        ]);

        return redirect()->route('room.index');
    }
}
