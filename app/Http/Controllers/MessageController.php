<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\CreateMessageRequest;

class MessageController extends Controller
{
    /**
     * @param CreateMessageRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateMessageRequest $request) : JsonResponse
    {
        $message = Message::create($request->all());

        $message = Message::find($message->id);

        return response()->json($message, 201);
    }

    /**
     * @param Request $request
     * @param string $roomId
     *
     * @return JsonResponse
     */
    public function getMessagesByRoomId(Request $request, string $roomId) : JsonResponse
    {
        $messages = Message::where('room_id', $roomId)->get();

        return response()->json($messages, 200);
    }
}
