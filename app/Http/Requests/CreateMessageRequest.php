<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required',
            'sender_id' => 'required|exists:users,id',
            'recipient_id' => 'exists:users,id',
            'room_id' => 'required_if:recipient_id,NULL|exists:users,id'
        ];
    }
}
