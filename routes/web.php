<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/', ['as' => 'room.index','uses' => 'RoomController@index']);
    Route::get('rooms/{room}', ['as' => 'room.show','uses' => 'RoomController@show'])->where('room', '[0-9]');
    Route::get('rooms/create', ['as' => 'room.create','uses' => 'RoomController@create']);
    Route::post('rooms/create', ['as' => 'room.store','uses' => 'RoomController@store']);

    Route::post('messages/create', ['as' => 'message.store','uses' => 'MessageController@store']);
    Route::get('messages/room/{roomId}', ['as' => 'message.room','uses' => 'MessageController@getMessagesByRoomId']);
});