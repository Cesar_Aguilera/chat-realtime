@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <chat :room="{{ $room }}" :user="{{ Auth::User() }}"></chat>
            </div>
        </div>
    </div>
@endsection