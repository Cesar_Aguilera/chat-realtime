@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Salas</div>
                    <div class="panel-body">
                        @if($rooms->isEmpty())
                            <p>
                                No se ha creado ninguna sala de chat! <a href="{{ route('room.create') }}">Crear una</a> ahora.
                            </p>
                        @else
                        <ul class="list-group">
                            @foreach($rooms as $room)
                                <li class="list-group-item">
                                    <a href="{{ route('room.show', [$room->id]) }}">
                                        {{ $room->name }}
                                    </a>
                                    <span class="pull-right">{{ $room->created_at->diffForHumans() }}</span>
                                </li>
                            @endforeach
                        </ul>
                            {!! $rooms->render() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection